import sys
import os
from subprocess import call

#this should be run from inside the data directory

def writeFiles(dir):
	process = list();
	process.append(dir);
	fx = open('commands.txt','w')
	while(len(process) > 0):	
		dir = process.pop();
		files = os.listdir(dir)
		for e in files:
			if(os.path.isdir(os.path.join(dir,e))):
				process.append(os.path.join(dir,e))
				continue
			fn = os.path.splitext(e)
			if(fn[1] == '.wav'):
				p2 = (os.path.join(dir,fn[0] + ".raw ")).replace("\\","/")
				p1 = (os.path.join(dir,fn[0] + ".wav ")).replace("\\","/")
				command = "sox -V -r 44100  -c 2 -s " + "./" + p1 + " -c 1 ./" + p2 
				print command
				fx.write(command)
				fx.write("; ")
				fx.write("mv " + "./" + p2 + " ./" + p1)
				fx.write("; ")
				

def main(argv):
    writeFiles(argv[0])
if __name__ == "__main__":
    main(sys.argv[1:])
