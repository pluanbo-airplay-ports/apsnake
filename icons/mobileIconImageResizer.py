"""
this script resizes argv into all the image sizes specified in the sizes variable
"""

from PIL import Image
import sys
import getopt
def resize(argv):
    sizes = [(72,72),(57,57),(48,48),(36,36),(29,29),(50,50),(58,58),(114,114)]
    im = Image.open(argv);
    im.load()
    for e in sizes:
        im.resize(e,Image.NEAREST).save(str(e[0])+ "x" + str(e[1]) + ".png")
