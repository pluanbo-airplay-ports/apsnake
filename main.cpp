#define BATCH
#define STOP_ALL
#include "QuGlobals.h"
#include "s3e.h"
#include "GuiAp.h"
#include "IwSound.h"
#include "IwResHandlerWAV.h"
#include "IwResource.h"
#include "IwSound.h"
#include "IwGx.h"
#include "QuUtils.h"
#include "QuGameManager.h"


#include "Global.h"
#include "SuiAp.h"
#include "APMain.h"
#include "s3eKeyboard.h"
#include "QuMouse.h"


static const float F_TAP_DISTANCE_THRESHOLD = 0.015f;		//distance grace for a "tapping" gesture
static const float F_TAP_TIME_THRESHOLD = 200.0f; //ms grace for a "tapping" gesture
static const float F_SWIPE_THRESHOLD = 0.03f; //distance threshold for a "swiping" gesture
static const float MOUSE_MOVE_GRACE = 300; // ms backtracking on mouse gestures

GuiKeyType mapDirToGuiKeyP2(GDirectionalInput aInput)
{
	if(aInput & G_LEFT)
		return GuiKeyType('j');
	else if(aInput & G_UP)
		return GuiKeyType('i');
	else if(aInput & G_RIGHT)
		return GuiKeyType('l');
	else if(aInput & G_DOWN)
		return GuiKeyType('k');
	
	quAssert(false);
	return GuiKeyType(0);
}

GuiKeyType mapDirToGuiKeyP1(GDirectionalInput aInput)
{
	if(aInput & G_LEFT)
		return GUI_LEFT;
	else if(aInput & G_UP)
		return GUI_UP;
	else if(aInput & G_RIGHT)
		return GUI_RIGHT;
	else if(aInput & G_DOWN)
		return GUI_DOWN;
	
	quAssert(false);
	return GuiKeyType(0);
}

void ApMapKeys(std::vector<GuiKeyType>& vApMapper)
{
	//this should take AP keys as index have GUI keys as value
    vApMapper.resize(400);
	vApMapper[s3eKeyLeft] = GUI_LEFT;
	vApMapper[s3eKeyRight] = GUI_RIGHT;
	vApMapper[s3eKeyUp] = GUI_UP;
	vApMapper[s3eKeyDown] = GUI_DOWN;
	vApMapper[s3eKeySpace] = GuiKeyType(32);
	vApMapper[111] = GuiKeyType(92);
	vApMapper[s3eKeyEnter] = GUI_RETURN;
	vApMapper[s3eKeyT] = GuiKeyType('t');
	vApMapper[s3eKeyM] = GuiKeyType('m');

	vApMapper[s3eKeyF1] = GUI_F1;
	vApMapper[s3eKeyF2] = GUI_F2;
	vApMapper[s3eKeyF3] = GUI_F3;
	vApMapper[s3eKeyF4] = GUI_F4;
	vApMapper[s3eKeyF5] = GUI_F5;
	vApMapper[s3eKeyF6] = GUI_F6;
	vApMapper[s3eKeyF7] = GUI_F7;
	vApMapper[s3eKeyF8] = GUI_F8;
	vApMapper[s3eKeyF9] = GUI_F9;
	vApMapper[s3eKeyF10] = GUI_F10;

	vApMapper[s3eKeyAbsGameA] = GuiKeyType(32);
	vApMapper[s3eKeyAbsGameB] = GuiKeyType(32);
	vApMapper[s3eKeyAbsGameC] = GuiKeyType(32);
	vApMapper[s3eKeyAbsGameD] = GuiKeyType(32);

	vApMapper[s3eKeyAbsBSK] = GuiKeyType(GUI_ESCAPE); //circle
	vApMapper[s3eKeyAbsOk] = GuiKeyType(32); //cross
}

class GenGuiManager : public QuBaseManager
{
	bool mInit;
	SP<GlobalController> pGl;
	SP<ApGraphicalInterface> pGraph;
	std::vector<GuiKeyType> vApMapper;

	QuAdvancedMultiTouchManager mTouches;
	std::map<int,GDirectionalInput> mLastDirection;	//note here unset entries default to 0 (G_DIRECTION_NEUTRAL) and we use this fact...
public:
	GenGuiManager():mInit(false)
	{
	}
	~GenGuiManager()
	{		
		
	}
	void guiInit()
	{
		srand( (unsigned)time( NULL ));
		try
		{
			ProgramInfo inf = GetProgramInfo();
			Rectangle sBound = Rectangle(Point(0, 0), inf.szScreenRez);
			//Rectangle sBound = Rectangle(Point(0, 0), Point(G_SCREEN_WIDTH,G_SCREEN_HEIGHT));
			pGraph = new ApGraphicalInterface(sBound.sz);
			SP< GraphicalInterface<Index> > pGr = new SimpleGraphicalInterface<ApImage*>(pGraph);
			SP<ApSoundInterface> pSound = new ApSoundInterface();
			SP< SoundInterface<Index> > pSndMng = new SimpleSoundInterface<ApSoundObject> (pSound);
			pGr->DrawRectangle(sBound, Color(0,0,0), true);
			if(inf.bMouseCapture)
			{
			}

			ProgramEngine pe(new EmptyEvent(), pGr, pSndMng, new IoWriter());
			pGl = GetGlobalController(pe);
			std::cout << "global controller constructed" << std::endl;
			ApMapKeys(vApMapper);
			Point pMousePos = Point(0,0);

		}
		catch(MyException& me)
		{
			std::cout << "exceptin in main setup: " << me.GetDescription(true) << "\n";
		}
		catch(...)
		{
			std::cout << "Unknown error!\n";
		}
	}
	virtual void initialize()
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//glDepthMask(false);
		//glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		glClearColor(0,0,0,1);

		guiInit();
		//test();
		mInit = true;
	}
	virtual void update()
	{
		clearScreen();
		runGui();
		pGraph->draw();
		
	}
	virtual void keyDown(int key)
	{ 
		if(key == s3eKeyAbsOk || key == s3eKeySpace) // x
		{
			sendButton(GUI_F1);
			sendButton(GuiKeyType(32));
		}
		else
			sendButton(vApMapper[key]); 
	}
	//virtual void keyUp(int key){ pGl->KeyUp(vApMapper[key]); }
	virtual void singleDown(int button, QuScreenCoord crd){}
	virtual void singleUp(int button, QuScreenCoord crd){}
	virtual void singleMotion(QuScreenCoord crd){}

	void sendButton(GuiKeyType key)
	{
		pGl->KeyDown(key);
		pGl->KeyUp(key);
	}
	void sendClick(QuVector2<int> p)
	{
		//std::cout << p.x << " " << p.y << std::endl;
		pGl->MouseClick(Gui::Point(p.x,p.y));
	}
	void multiTouch(int button, bool state,  QuScreenCoord crd)
	{
		if(state)
		{
			mTouches.multiTouchDown(button,crd);
			mTouches.multiMotion(button,crd);
		}

		if(state)
		{
			QuScreenCoord conv = pGraph->convertScreenToCameraCoord(crd);
			conv.rotate(pGraph->getCurrentCameraRotation());
			conv.reflect(G_SCREEN_REFLECT_VERTICAL);
			sendClick(conv.getIPosition());		
		}
		else
		{
			if(mTouches.getTimeDown(button) < F_TAP_TIME_THRESHOLD && mTouches.getTotalFMouseChange(button,G_DR_0).magnitude() < F_TAP_DISTANCE_THRESHOLD)
			{
				GDeviceRotation rot = pGraph->getCurrentCameraRotation();
				if(!mTouches.oldest(button).doesBoxContain(QuBox2<float>(0,0,0.5,0.5),rot))
				{
					sendButton(GuiKeyType(32));
				}
				else 
				{
					sendButton(GUI_RETURN);
				}
			}
			mTouches.multiTouchUp(button);
		}
		mLastDirection[button] = G_DIRECTION_NEUTRAL;
	}

	void multiMotion(int button,  QuScreenCoord crd)
	{
		mTouches.multiMotion(button,crd);
		GDeviceRotation rot = pGraph->getCurrentCameraRotation();
		GDirectionalInput dir = mTouches.getDirectionalMouseChange(button, MOUSE_MOVE_GRACE, F_SWIPE_THRESHOLD, rot);
		if(dir != mLastDirection[button]) 
		{
			mLastDirection[button] = dir;
			std::cout << "turning in direction " << dir << std::endl;
			
			//TODO test
			//are we in left quadrant?
			if(dir != G_DIRECTION_NEUTRAL)
			{
				if(!mTouches.oldest(button).doesBoxContain(QuBox2<float>(0,0,0.5,0.5),rot))
				{
					sendButton(mapDirToGuiKeyP1(dir));
				}
				else //then we must be in right quadrant
				{
					sendButton(mapDirToGuiKeyP2(dir));
				}
			}
		}
	}

	void clearScreen()
	{
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	}
	void runGui()
	{
		try{
			pGl->Update();
		}
		catch(...){
			std::cout << "exception" << std::endl;
			s3eDeviceExit();
		}	
	}
	void test()
	{
		ApGraphicalInterface * gi = new ApGraphicalInterface(Size(G_SCREEN_HEIGHT,G_SCREEN_WIDTH));
		//ApGraphicalInterface * gi = pGraph.GetRawPointer();
		//ApImage * img = gi->GetBlankImage(Size(256,256));
		ApImage * img  = gi->LoadImage("tower.bmp");
		for(int i = 0; i < 1; i++)
			for(int j = 0; j < 1; j++)
				img->SetPixel(Point(i,j),Color(255,127,0,255));
		gi->SaveImage("newtower.bmp",img);
		gi->DrawImage(Point(0,0),img,Rectangle(0,0,200,200));
		gi->DrawRectangle(Rectangle(10,10,20,20),Color(0,255,0,255));
		delete img;
	}
};

int main()
{
	
	QuStupidPointer<QuGlobalSettings> s = new QuGlobalTypeSettings<QuApRawSoundManager<22050>,QuImageManager,ApFileManager>();
	s->mRefresh = true;
	INITIALIZE_GAME<GenGuiManager>(s);
	//return APMain(new GenGuiManager());
}

